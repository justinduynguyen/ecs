﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCube : MonoBehaviour
{
    public float speed;
    public float alpha;
    private void Start()
    {
        speed = Random.Range(5, 25);
        alpha = Random.Range(10, 180);
    }
    // Update is called once per frame
    void Update()
    {
        transform.position += transform.position.normalized * Mathf.Sin(Time.time * speed + alpha) * Time.deltaTime * 25;
    }
}
