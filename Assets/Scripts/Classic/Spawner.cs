﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using Unity.Jobs;
public class Spawner : MonoBehaviour
{
    EntityManager entityManager;
    [SerializeField]
    private Mesh sharedMesh;
    [SerializeField]
    private Material sharedMaterial;
    [SerializeField]
    GameObject gamePrefab, classicGameObject;
    [SerializeField]
    int entityAmount;
    [SerializeField]
    bool isClassic;
    private void Start()
    {
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        if (!isClassic)
            CreateEntity();
        else
        {

            CreateGameObject();
        }
    }
    void CreateGameObject()
    {
        for (int i = 0; i < entityAmount; i++)
        {

            Instantiate(classicGameObject, UnityEngine.Random.insideUnitSphere * 10, Quaternion.identity);
        }
    }

    void CreateEntity()
    {
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, new BlobAssetStore());
        Entity entityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(gamePrefab, settings);
        for (int i = 0; i < entityAmount; i++)
        {
            Entity entity = entityManager.Instantiate(entityPrefab);
            entityManager.AddComponentData(entity, new Translation() { Value = new float3(UnityEngine.Random.insideUnitSphere) * 10 });
            entityManager.AddComponentData(entity, new SpeedComponentData()
            {
                speed = UnityEngine.Random.Range(5, 25),
                alpha = UnityEngine.Random.Range(10, 180)
            });
        }
    }


}
