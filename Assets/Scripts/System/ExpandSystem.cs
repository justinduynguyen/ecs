﻿
using Unity.Entities;
using Unity.Transforms;
using Unity.Burst;
using Unity.Mathematics;

public class ExpandSystem : SystemBase
{

    protected override void OnUpdate()
    {
        float dt = Time.DeltaTime;
        float t = (float)Time.ElapsedTime;
        Entities.ForEach((ref Translation trans, in SpeedComponentData speedCom) =>
        {
            trans.Value += math.normalize(trans.Value) * math.sin(t * speedCom.speed+speedCom.alpha) * dt * 25;

        }).WithBurst(FloatMode.Fast, FloatPrecision.High, false).Schedule();
    }
}