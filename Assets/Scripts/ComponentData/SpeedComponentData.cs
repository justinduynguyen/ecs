﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct SpeedComponentData : IComponentData
{
    public float speed;
    public float alpha;
}
